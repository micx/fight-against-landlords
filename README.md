# fight-against-landlords

#### 介绍
1. 基于netty websocket和kaka-core事件领域模型实现，前后端通信格式为protobuf。

2. 支持四种玩法，常规、不洗牌（多炸弹）、癞子、连炸（两个或以上的炸弹牌点连续，可压炸王炸）。

3. 对局阶段的通信协议为队列化执行，可基本消除锁竞争机制，显著增加并发量和在线用户量，在此基础上，普通单服用户承载量可达几千上万。

4. 玩家对局为自动匹配机制，癞子玩法不匹配机器人，其它玩法优先匹配真人，无足够真人进入游戏时自动匹配机器人。

5. 机器人部分为普通算法实现，非深度学习算法。

6. 体验地址 http://101.34.22.36:8080/

![大厅 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102027.png)
![进入游戏加载必要的资源 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102050.png)
![自动匹配玩家 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102100.png)
![游戏托管 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102111.png)
![不洗牌玩法游戏中 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102117.png)
![游戏结算 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102123.png)
![癞子玩法游戏中 ](https://gitee.com/zkpursuit/fight-against-landlords/raw/master/images/20211027102129.png)

通告：本项目为学习参考型半成品工程，不涉及任何广告、盈利或其它经济利益。

#### 软件架构
基于WebSocket状态同步，后端使用kaka-core事件驱动框架（ https://gitee.com/zkpursuit/kaka-core ），前后端通信格式为：协议号 + protobuf，通过协议号分发到事件驱动器进行业务处理。

#### 使用说明
1. 本项目为maven工程，在IDE中导入本项目源码。
   
2. 建立数据库，导入ddz-ws-server resources下的ddz.sql并执行。
   
3. 配置ddz-ws-server resources下的db.properties，进行数据库连接配置。
   
4. 配置ddz-ws-server resources下的redis.properties，进行redis缓存配置。
   
5. 运行ddz.Main类。
