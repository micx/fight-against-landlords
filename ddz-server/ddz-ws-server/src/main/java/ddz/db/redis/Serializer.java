package ddz.db.redis;

public interface Serializer<T> {

    byte[] serialize(T obj);

    T deserialize(byte[] bytes);

}
