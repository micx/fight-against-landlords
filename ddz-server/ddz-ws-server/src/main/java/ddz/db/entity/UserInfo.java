package ddz.db.entity;

import java.io.Serializable;

public class UserInfo implements Serializable {

    private Long uid;
    private String openid;
    private String unionid;
    private int sex;
    private String nickname;
    private String headImg;
    private int winCount;
    private int totalCount;
    private int score;
    private int gold;
    private int diam;
    private int fuka;
    private int serialWin;
    private int maxSeriaWin;
    private int lianWin;
    private int lastLianWin;
    private long lianWinDesk;
    private int lianWinReviveCount;
    private int maxLianWin;
    private long deskId;
    private long createTimestamp; //创建时间戳
    private long lastLoginTimestamp; //最后登录时间戳
    private String items;
    private String token;
    private String dailySign;
    private Long signDate;
    private Long welfareDate;
    private int benefitCount;
    private Long benefitDate;
    private Long firstRechDate;
    private Long inviteAwardsDate;
    private String playCounts;

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public int getWinCount() {
        return winCount;
    }

    public void setWinCount(int winCount) {
        this.winCount = winCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getDiam() {
        return diam;
    }

    public void setDiam(int diam) {
        this.diam = diam;
    }

    public int getFuka() {
        return fuka;
    }

    public void setFuka(int fuka) {
        this.fuka = fuka;
    }

    public int getSerialWin() {
        return serialWin;
    }

    public void setSerialWin(int serialWin) {
        this.serialWin = serialWin;
    }

    public int getMaxSeriaWin() {
        return maxSeriaWin;
    }

    public void setMaxSeriaWin(int maxSeriaWin) {
        this.maxSeriaWin = maxSeriaWin;
    }

    public int getLianWin() {
        return lianWin;
    }

    public void setLianWin(int lianWin) {
        this.lianWin = lianWin;
    }

    public int getLastLianWin() {
        return lastLianWin;
    }

    public void setLastLianWin(int lastLianWin) {
        this.lastLianWin = lastLianWin;
    }

    public long getLianWinDesk() {
        return lianWinDesk;
    }

    public void setLianWinDesk(long lianWinDesk) {
        this.lianWinDesk = lianWinDesk;
    }

    public int getLianWinReviveCount() {
        return lianWinReviveCount;
    }

    public void setLianWinReviveCount(int lianWinReviveCount) {
        this.lianWinReviveCount = lianWinReviveCount;
    }

    public int getMaxLianWin() {
        return maxLianWin;
    }

    public void setMaxLianWin(int maxLianWin) {
        this.maxLianWin = maxLianWin;
    }

    public long getDeskId() {
        return deskId;
    }

    public void setDeskId(long deskId) {
        this.deskId = deskId;
    }

    public long getCreateTimestamp() {
        return createTimestamp;
    }

    public void setCreateTimestamp(long createTimestamp) {
        this.createTimestamp = createTimestamp;
    }

    public long getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp(long lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDailySign() {
        return dailySign;
    }

    public void setDailySign(String dailySign) {
        this.dailySign = dailySign;
    }

    public Long getSignDate() {
        return signDate;
    }

    public void setSignDate(Long signDate) {
        this.signDate = signDate;
    }

    public Long getWelfareDate() {
        return welfareDate;
    }

    public void setWelfareDate(Long welfareDate) {
        this.welfareDate = welfareDate;
    }

    public int getBenefitCount() {
        return benefitCount;
    }

    public void setBenefitCount(int benefitCount) {
        this.benefitCount = benefitCount;
    }

    public Long getBenefitDate() {
        return benefitDate;
    }

    public void setBenefitDate(Long benefitDate) {
        this.benefitDate = benefitDate;
    }

    public Long getFirstRechDate() {
        return firstRechDate;
    }

    public void setFirstRechDate(Long firstRechDate) {
        this.firstRechDate = firstRechDate;
    }

    public Long getInviteAwardsDate() {
        return inviteAwardsDate;
    }

    public void setInviteAwardsDate(Long inviteAwardsDate) {
        this.inviteAwardsDate = inviteAwardsDate;
    }

    public String getPlayCounts() {
        return playCounts;
    }

    public void setPlayCounts(String playCounts) {
        this.playCounts = playCounts;
    }
}
