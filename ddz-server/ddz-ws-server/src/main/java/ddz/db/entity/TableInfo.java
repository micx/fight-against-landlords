package ddz.db.entity;

import ddz.core.*;
import ddz.core.dealer.Dealer;
import ddz.core.dealer.LianZhaDealer;
import ddz.core.dealer.NoShuffleDealer;
import ddz.utils.JsonUtils;

import java.io.Serializable;
import java.util.List;

/**
 * 数据库存储的牌桌数据
 */
public class TableInfo extends BaseTable implements Serializable {

    private String players;
    private String records;
    private int state;

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public String getRecords() {
        return records;
    }

    public void setRecords(String records) {
        this.records = records;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Table toDesk() {
        Table table = new Table(this.id, this.maxPlayers);
        table.setMode(this.mode);
        table.setPlace(this.place);
        if (table.getMode() == GameMode.NO_SHUFFLE.getId()) {
            table.setDealer(new NoShuffleDealer(1));
        } else if (table.getMode() == GameMode.LIAN_ZHA.getId()) {
            table.setDealer(new LianZhaDealer(1));
        } else {
            table.setDealer(new Dealer(1));
        }
        table.setBankerSeatIndex(this.bankerSeatIndex);
        table.setBaseMultiple(this.baseMultiple);
        Player[] players = JsonUtils.parseObject(this.players, Player[].class);
        for (int i = 0; i < players.length; i++) {
            table.addPlayer(players[i]);
        }
        List<OptRecord> list = (List<OptRecord>) JsonUtils.parseObject(this.records, List.class, OptRecord.class);
        list.forEach((OptRecord record) -> {
            table.addOptRecord(record);
        });
        table.setCreateTimestamp(this.createTimestamp);
        table.setStep(this.step);
        TableState[] states = TableState.values();
        for (TableState state : states) {
            if (state.getValue() == this.getState()) {
                table.setState(state);
                break;
            }
        }
        table.setFapaiStartTime(this.fapaiStartTime);
        table.setLaiziPoint(this.laiziPoint);
        return table;
    }
}
