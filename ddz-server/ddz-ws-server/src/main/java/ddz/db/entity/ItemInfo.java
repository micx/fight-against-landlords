package ddz.db.entity;

/**
 * 玩家道具
 *
 * @author zkpursuit
 */
public class ItemInfo {

    private int cid;
    private int num;
    private long timestamp; //拥有道具的起始时间，相对有时间限制的道具

    public ItemInfo() {
    }

    public ItemInfo(int cid, int num) {
        this.cid = cid;
        this.num = num;
    }

    public ItemInfo(int cid, int num, long timestamp) {
        this.cid = cid;
        this.num = num;
        this.timestamp = timestamp;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
