package ddz.db.service;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * 匹配池中的成员对象
 */
public class MatchMember implements Serializable {

    public long uid;
    public long startMatchTime;
    public int score;
    public Map<String, Object> params;

    public MatchMember(long uid, int score, Map<String, Object> params) {
        this.uid = uid;
        this.score = score;
        this.startMatchTime = System.currentTimeMillis();
        this.params = params;
    }

    public long getUid() {
        return uid;
    }

    public long getStartMatchTime() {
        return startMatchTime;
    }

    public int getScore() {
        return score;
    }

    public Map<String, Object> getParam() {
        return params;
    }

    @Override
    public String toString() {
        return "MatchMember{" +
                "uid=" + uid +
                ", startMatchTime=" + startMatchTime +
                ", score=" + score +
                ", params=" + params +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MatchMember)) return false;
        MatchMember that = (MatchMember) o;
        return getUid() == that.getUid() &&
                getStartMatchTime() == that.getStartMatchTime() &&
                getScore() == that.getScore();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUid());
    }
}
