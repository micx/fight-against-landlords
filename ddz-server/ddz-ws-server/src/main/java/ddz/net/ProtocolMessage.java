package ddz.net;

import com.kaka.notice.Message;
import io.netty.channel.ChannelHandlerContext;

/**
 * 通信协议消息转发
 *
 * @author zkpursuit
 */
public class ProtocolMessage extends Message {

    ChannelHandlerContext ctx;
    Long uid;

    public ProtocolMessage(Object what, Object data, ChannelHandlerContext ctx) {
        super(what, data);
        this.ctx = ctx;
        Object ctxId = CtxManager.getBindId(this.ctx);
        if (ctxId != null && !(ctxId instanceof String)) {
            uid = (Long) ctxId;
        }
    }

    public ProtocolMessage(Object what, Object data, Object user) {
        super(what, data);
        if(user != null) {
            if (user instanceof ChannelHandlerContext) {
                this.ctx = (ChannelHandlerContext) user;
                Object ctxId = CtxManager.getBindId(this.ctx);
                if (ctxId != null && !(ctxId instanceof String)) {
                    uid = (Long) ctxId;
                }
            } else if (user instanceof Long) {
                this.uid = (Long) user;
                this.ctx = CtxManager.instance().get(this.uid);
            }
        }
    }

    public ChannelHandlerContext ctx() {
        return ctx;
    }

    public Long uid() {
        return uid;
    }

    public void setWhat(Object what) {
        this.what = what;
    }

    public void setData(Object data) {
        this.body = data;
    }

    @Override
    public void reset() {
        super.reset();
        this.ctx = null;
        this.uid = null;
    }

}
