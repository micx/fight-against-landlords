package ddz.core.ai;

enum WeightType {
    dan(1), //单张
    dui(2), //一对
    san(3), //三张
    zha(7), //炸弹
    danShun(4), //顺子 每多一张牌权值+1
    duiShun(5), //连对 每多一对牌，权值+2
    sanShun(6); //飞机 每多一飞机，权值在基础上+3

    final int weight;

    WeightType(int weight) {
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }
}
