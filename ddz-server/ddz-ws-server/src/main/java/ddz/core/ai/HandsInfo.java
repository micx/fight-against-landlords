package ddz.core.ai;

import java.util.List;
import java.util.Map;

/**
 * 手数信息
 */
class HandsInfo {
    public final int weight;
    public final int hands;
    public final Map<WeightType, List<int[]>> map;

    HandsInfo(int weight, int hands, Map<WeightType, List<int[]>> map) {
        this.weight = weight;
        this.hands = hands;
        this.map = map;
    }
}
