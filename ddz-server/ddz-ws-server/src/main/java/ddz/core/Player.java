package ddz.core;

import com.kaka.util.ObjectPool;

import java.io.Serializable;

/**
 * 逻辑玩家
 */
public class Player implements Serializable, ObjectPool.Poolable {

    private Long uid;
    private int seatIndex;
    private int gold;
    private boolean ready;    //是否已准备
    private boolean priorLookDiPai; //是否可优先看底牌
    private Cards mycards;
    private int timeoutCount = 0; //操作超时次数
    private boolean usedCounter; //是否已使用记牌器

    public Player() {
        this.mycards = new Cards();
    }

    public Long getUid() {
        return uid;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public int getSeatIndex() {
        return seatIndex;
    }

    public void setSeatIndex(int seatIndex) {
        this.seatIndex = seatIndex;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public boolean isPriorLookDiPai() {
        return priorLookDiPai;
    }

    public void setPriorLookDiPai(boolean priorLookDiPai) {
        this.priorLookDiPai = priorLookDiPai;
    }

    public Cards getMycards() {
        return mycards;
    }

    public int getTimeoutCount() {
        return timeoutCount;
    }

    public void setTimeoutCount(int timeoutCount) {
        this.timeoutCount = timeoutCount;
    }

    public boolean isUsedCounter() {
        return usedCounter;
    }

    public void setUsedCounter(boolean usedCounter) {
        this.usedCounter = usedCounter;
    }

    @Override
    public void reset() {
        this.mycards.clear();
        this.ready = false;
        this.usedCounter = false;
        this.priorLookDiPai = false;
    }
}
