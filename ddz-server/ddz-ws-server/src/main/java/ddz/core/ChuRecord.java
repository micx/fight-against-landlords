package ddz.core;

import java.io.Serializable;

/**
 * 出牌操作记录，包括不出牌的记录
 *
 * @author zkpursuit
 */
public class ChuRecord extends OptRecord implements Serializable {

    private boolean finished = false; //是否出完牌
    private int type; //牌类型
    private int[] discardCards; //打出的牌

    public ChuRecord() {
        super();
    }

    public ChuRecord(OptType optType, int seatIndex) {
        super(optType, seatIndex);
    }

    public ChuRecord(OptType optType, int seatIndex, int[] discardCards, CardType type) {
        super(optType, seatIndex);
        setData(discardCards, type);
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public CardType getType() {
        return CardType.getType(type);
    }

    public void setType(CardType type) {
        this.type = type.getId();
    }

    public int[] getDiscardCards() {
        return discardCards;
    }

    public void setDiscardCards(int[] discardCards) {
        this.discardCards = discardCards;
    }

    private void setData(int[] discardCards, CardType type) {
        this.discardCards = discardCards;
        this.type = type.getId();
    }

    public final void clear() {
        if (discardCards != null) {
            discardCards = null;
        }
        type = CardType.none.getId();
    }

    public final boolean isEmpty() {
        if (discardCards == null) {
            return true;
        }
        return discardCards.length == 0;
    }

}
