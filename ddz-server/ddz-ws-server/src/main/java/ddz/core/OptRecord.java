package ddz.core;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * 操作记录
 *
 * @author zkpursuit
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "sign")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ChuRecord.class, name = "ChuRecord")
})
public class OptRecord implements Serializable {

    protected String sign;
    private int optType;
    private int seatIndex; //操作方座位索引
    private long timestamp; //时间戳

    public OptRecord() {
        sign = this.getClass().getSimpleName();
    }

    public OptRecord(OptType optType, int seatIndex) {
        this();
        this.optType = optType.getId();
        this.seatIndex = seatIndex;
        this.timestamp = System.currentTimeMillis();
    }

    public OptType getOptType() {
        return OptType.getType(this.optType);
    }

    public void setOptType(OptType optType) {
        this.optType = optType.getId();
    }

    public int getSeatIndex() {
        return seatIndex;
    }

    public void setSeatIndex(int seatIndex) {
        this.seatIndex = seatIndex;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
