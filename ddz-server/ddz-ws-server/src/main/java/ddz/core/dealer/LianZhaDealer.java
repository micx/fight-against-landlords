package ddz.core.dealer;

import com.kaka.util.IntArray;
import com.kaka.util.MathUtils;
import ddz.core.Cards;
import ddz.core.Pile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 连炸洗牌算法
 */
public class LianZhaDealer extends Dealer {

    public LianZhaDealer(int several) {
        super(several);
    }

    protected void shuffle() {
        cards.clear();
        init();
        Cards allCards = new Cards();
        for (Integer card : cards) {
            allCards.addCard(card);
        }
        cards.clear();
        int maxZha = MathUtils.random(4, 11);
        List<IntArray> zhaList = new ArrayList<>();
        int zhaCount = 0;
        int k = allCards.getMinValidIndex();
        while (k < allCards.getMaxValidIndex()) {
            if (zhaCount >= maxZha) break;
            boolean flag = MathUtils.randomBoolean();
            if (flag) {
                if (maxZha - zhaCount > 2) {
                    int num = MathUtils.random(2, maxZha - zhaCount);
                    int start = k;
                    int end = k + num;
                    if (end > allCards.getMaxValidIndex()) {
                        end = allCards.getMaxValidIndex();
                    }
                    num = end - start + 1;
                    if (num > 0) {
                        IntArray lianZhaCards = new IntArray(4 * num);
                        for (int m = start; m <= end; m++) {
                            Pile pile = allCards.removePile(m);
                            lianZhaCards.addAll(pile.getCards());
                            zhaCount++;
                        }
                        zhaList.add(lianZhaCards);
                        k += num;
                    } else {
                        k++;
                    }
                } else {
                    k++;
                }
            } else {
                k++;
            }
        }
        //System.out.println("炸弹数量：" + zhaCount);
        //for (IntArray cs : zhaList) {
        //    System.out.println(Arrays.toString(cs.toArray()));
        //}
        int[] _cards = allCards.getDesCards();
        for (int card : _cards) {
            cards.add(card);
        }
        Collections.shuffle(cards);
        IntArray cardsClone = new IntArray(cards.size());
        for (Integer card : cards) {
            cardsClone.add(card);
        }
        cards.clear();
        for (int m = 0; m < cardsClone.size(); m++) {
            boolean flag = MathUtils.randomBoolean();
            if (flag && !zhaList.isEmpty()) {
                IntArray lianZhaCards = zhaList.remove(zhaList.size() - 1);
                for (int j = 0; j < lianZhaCards.size(); j++) {
                    cards.add(lianZhaCards.get(j));
                }
            }
            int card = cardsClone.get(m);
            cards.add(card);
        }
        while (!zhaList.isEmpty()) {
            IntArray lianZhaCards = zhaList.remove(zhaList.size() - 1);
            for (int j = 0; j < lianZhaCards.size(); j++) {
                cards.add(lianZhaCards.get(j));
            }
        }
        //System.out.println(cards.size());
    }

    private int[] amounts = new int[]{8, 12, 16};

    public void distribute(Cards[] array, int remaing) {
        shuffle();
        int idx = MathUtils.random(0, amounts.length - 1);
        int oneAmount = amounts[idx];
        distribute(array, remaing, oneAmount);
    }

//    public static void main(String[] args) {
//        LianZhaDealer dealer = new LianZhaDealer(1);
//        HandCards[] array = new HandCards[3];
//        for (int i = 0; i < array.length; i++) {
//            array[i] = new HandCards();
//        }
//        dealer.distribute(array, 3);
//        for (int i = 0; i < array.length; i++) {
//            System.out.println(Arrays.toString(array[i].getCards()));
//        }
//    }

}
