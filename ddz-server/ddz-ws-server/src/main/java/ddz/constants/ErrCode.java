package ddz.constants;

/**
 * 错误码
 *
 * @author zkpursuit
 */
public enum ErrCode {

    login_token_invalid(300, "登录令牌失效，请重新登录"),
    login_invalid(301, "被抢线登录"),
    frequent_operate(399, "您的手速太快了……"),
    illegal_request(400, "非法数据请求，请先登录"),
    no_join_room(401, "未加入任何房间"),
    room_playing(402, "牌局进行中"),
    no_discard_player(403, "非出牌方不能操作"),
    joined_room(404, "已加入其它房间"),
    room_not_exist(405, "房间不存在或已被解散"),
    illegal_operation(406, "非法操作"),
    room_not_playing(407, "牌局未开始"),
    had_friend(408, "已有朋友"),
    not_hava_card(409, "不存在的牌数据"),
    cannot_confirm_friend(410, "不符合规则的牌不能确定朋友"),
    player_readied(411, "玩家已准备"),
    banker_friend_no_confirm(412, "庄家朋友未确定"),
    card_type_err(413, "牌型错误"),
    not_over_pre_card(414, "不能压死上家的牌"),
    cannot_pass_self(415, "此轮首个出牌方"),
    cd_expire(416, "倒计时已过期"),
    room_full(417, "房间满员"),
    create_room_faild(418, "创建房间失败"),
    repeat_exit_room(419, "不能重复退出房间"),
    game_ground_not_exist(420, "游戏场不存在"),
    gold_not_enough(421, "游戏币不足"),
    diam_not_enough(422, "钻石不足"),
    item_not_enough(423, "道具不足"),
    no_benefit(424, "今日补助已领完"),
    no_benefit_conditions(425, "不满足领取补助条件"),
    not_opened_recharge_service(425, "未开放的充值服务"),
    not_rebuy_first_recharge(426, "不可重复购买首充礼包"),
    email_annex_geted(426, "附件奖励已被领取"),
    email_overdue(427, "邮件已过期"),
    jiabei_not_enough(428, "超级加倍卡不足"),
    email_del_not_exists(429, "预删除的邮件不存在"),
    gold_beyond_max_limit(430, "您的技术太好了，请选择更高级别的场次"),
    lian_win_revive_diam_not_enough(431, "钻石不足，无法完成本次复活"),
    lian_win_max(432, "您技术超群，连胜次数已达最高领奖限制，可领取奖励后继续挑战"),
    dipaiKa_not_enough(433, "底牌卡不足"),
    cansaiKa_not_enough(434, "参赛卡不足"),
    lian_win_revive_max(435, "此轮连胜赛已达最大复活次数"),
    data_err(500, "数据错误"),
    player_data_not_exist(501, "玩家数据不存在或已在其它设备登录"),
    interface_disenable(502, "接口未启用"),
    update_data_err(503, "更新数据错误"),
    already_sign(600, "不可重复签到"),
    user_sign_err(601, "已签到天数与用户签到不一致"),
    sign_expire(602, "操作间隔超出一天，请重新签到"),
    already_welfare(603, "已领取今日福利"),
    task_target_not_achieved(604, "未达成目标，不能领取奖励"),
    task_activity_not_achieved(605, "活跃度值未达目标，不能领取奖励"),
    task_rewards_geted(606, "奖励已领取"),
    task_activity_order_get_rewards(607, "须按顺序领取"),
    friend_invite_not_enough(608, "邀请并成功进入游戏的用户数量不足"),
    friend_invite_rewards_getted(609, "已领取邀请奖励"),
    friend_cannot_get_self_play_rewards(610, "好友关系作为被邀请者不能领取自己的对局次数奖励"),
    friend_play_count_not_enough(611, "被邀请好友对局次数不足，不能领取奖励"),
    friend_give_diam_not_enough(612, "钻石不足，无法完成赠送"),
    friend_isnot_canot_give(613, "非好友关系，不能赠送欢乐豆"),
    friend_data_not_exists(614, "好友数据不存在"),
    friend_play_awards_geted(615, "已领取好友的对局次数奖励");

    private final int code;
    private final String desc;

    /**
     * 枚举构造
     *
     * @param code 错误码
     * @param desc 错误描述
     */
    private ErrCode(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getInfo() {
        return desc;
    }

}
