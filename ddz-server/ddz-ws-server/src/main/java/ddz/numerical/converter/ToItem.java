package ddz.numerical.converter;

import com.kaka.numerical.annotation.NumericField;
import ddz.numerical.pojo.Item;

/**
 * @author zhoukai
 */
public class ToItem implements NumericField.Converter<Item> {

    @Override
    public Item transform(String data) {
        return toItem(data);
    }

    public final static Item toItem(String data) {
        if (data == null) {
            return null;
        }
        if ("".equals(data)) {
            return null;
        }
        if ("0".equals(data)) {
            return null;
        }
        if (data.length() == 0) {
            return null;
        }
        data = data.trim();
        String[] strs = data.split("#");
        int cid = Integer.parseInt(strs[0]);
        int num = Integer.parseInt(strs[1]);
        return new Item(cid, num);
    }
}
