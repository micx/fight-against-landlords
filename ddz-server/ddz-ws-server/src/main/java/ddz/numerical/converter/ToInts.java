package ddz.numerical.converter;

import com.kaka.numerical.annotation.NumericField;

public class ToInts implements NumericField.Converter<Integer[]> {
    @Override
    public Integer[] transform(String data) {
        if (data == null) {
            return null;
        }
        if ("".equals(data)) {
            return null;
        }
        data = data.trim();
        String[] strs = data.split("[;,，]");
        Integer[] array = new Integer[strs.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = Integer.parseInt(strs[i]);
        }
        return array;
    }
}
