package ddz.numerical.manager;

import com.kaka.numerical.annotation.Numeric;
import ddz.numerical.ConfManager;
import ddz.numerical.pojo.ConfTimeoutInfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Numeric(src = "conf_timeout.txt")
public class ConfTimeoutInfoManager extends ConfManager<ConfTimeoutInfo> {

    private final Map<String, ConfTimeoutInfo> map = new ConcurrentHashMap<>();
    private final List<ConfTimeoutInfo> list = Collections.synchronizedList(new ArrayList<>());

    @Override
    protected void cacheObject(ConfTimeoutInfo confTimeoutInfo) {
        map.put(confTimeoutInfo.getName(), confTimeoutInfo);
        list.add(confTimeoutInfo);
    }

    @Override
    protected void parseBefore() {
        map.clear();
        list.clear();
    }

    @Override
    protected void parseAfter() {
    }

    public List<ConfTimeoutInfo> getList() {
        return list;
    }

    public ConfTimeoutInfo getJiaBeiInfo() {
        return map.get("jia_bei");
    }

    public ConfTimeoutInfo getDiZhuInfo() {
        return map.get("di_zhu");
    }

    public ConfTimeoutInfo getChuOrPassInfo() {
        return map.get("chu_or_pass");
    }

}
