package ddz.numerical.pojo;

public class ConfTimeoutInfo {

    private String name;
    private long millsesc;
    private long warnMillsec;
    private long timerOffset;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMillsesc() {
        return millsesc;
    }

    public void setMillsesc(long millsesc) {
        this.millsesc = millsesc;
    }

    public long getWarnMillsec() {
        return warnMillsec;
    }

    public void setWarnMillsec(long warnMillsec) {
        this.warnMillsec = warnMillsec;
    }

    public long getTimerOffset() {
        return timerOffset;
    }

    public void setTimerOffset(long timerOffset) {
        this.timerOffset = timerOffset;
    }
}
