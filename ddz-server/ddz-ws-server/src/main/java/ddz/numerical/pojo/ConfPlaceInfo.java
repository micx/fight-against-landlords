package ddz.numerical.pojo;

import com.kaka.numerical.annotation.NumericField;
import ddz.numerical.converter.ToItems;

import java.io.Serializable;
import java.util.List;

/**
 * 游戏场
 *
 * @author zkpursuit
 */
public class ConfPlaceInfo implements Serializable {

    private int id;
    private int mode;
    private int place;
    private String name;
    private int intoCost;
    private int baseGold;
    private int multiple;
    private int maxMultiple;
    private int minGold;
    private int maxGold;
    private int maxLoseGold;
    @NumericField(elements = {"give1"}, converter = ToItems.class)
    private List<Item> give1;
    @NumericField(elements = {"give2"}, converter = ToItems.class)
    private List<Item> give2;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIntoCost() {
        return intoCost;
    }

    public void setIntoCost(int intoCost) {
        this.intoCost = intoCost;
    }

    public int getBaseGold() {
        return baseGold;
    }

    public void setBaseGold(int baseGold) {
        this.baseGold = baseGold;
    }

    public int getMultiple() {
        return multiple;
    }

    public void setMultiple(int multiple) {
        this.multiple = multiple;
    }

    public int getMaxMultiple() {
        return maxMultiple;
    }

    public void setMaxMultiple(int maxMultiple) {
        this.maxMultiple = maxMultiple;
    }

    public int getMinGold() {
        return minGold;
    }

    public void setMinGold(int minGold) {
        this.minGold = minGold;
    }

    public int getMaxGold() {
        return maxGold;
    }

    public void setMaxGold(int maxGold) {
        this.maxGold = maxGold;
    }

    public int getMaxLoseGold() {
        return maxLoseGold;
    }

    public void setMaxLoseGold(int maxLoseGold) {
        this.maxLoseGold = maxLoseGold;
    }

    public List<Item> getGive1() {
        return give1;
    }

    public void setGive1(List<Item> give1) {
        this.give1 = give1;
    }

    public List<Item> getGive2() {
        return give2;
    }

    public void setGive2(List<Item> give2) {
        this.give2 = give2;
    }
}
