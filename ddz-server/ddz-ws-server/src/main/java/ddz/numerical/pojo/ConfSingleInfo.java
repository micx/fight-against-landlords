package ddz.numerical.pojo;

/**
 * 单独的配置数据
 */
public class ConfSingleInfo {
    private int maxReviveCount;
    private int benefitGold;

    public int getMaxReviveCount() {
        return maxReviveCount;
    }

    public void setMaxReviveCount(int maxReviveCount) {
        this.maxReviveCount = maxReviveCount;
    }

    public int getBenefitGold() {
        return benefitGold;
    }

    public void setBenefitGold(int benefitGold) {
        this.benefitGold = benefitGold;
    }
}
