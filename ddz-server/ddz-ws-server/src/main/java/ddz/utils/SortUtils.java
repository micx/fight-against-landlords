package ddz.utils;

public class SortUtils {

    public interface IntComparator {
        int compare(int v1, int v2);
    }

    /**
     * 插入排序
     *
     * @param array      待排序的数组
     * @param comparator 数值比较器
     * @return 排序后的数组
     */
    public static int[] sort(int[] array, IntComparator comparator) {
        for (int i = 1; i < array.length; i++) {
            //待插入元素
            int temp = array[i];
            int j;
            for (j = i - 1; j >= 0; j--) {
                //将大于temp的往后移动一位
                int result = comparator.compare(array[j], temp);
                if (result > 0) {
                    array[j + 1] = array[j];
                } else {
                    break;
                }
            }
            array[j + 1] = temp;
        }
        return array;
    }

//    public static void main(String[] args) {
//        int[] arr = new int[]{203, 304, 205};
//        sort(arr, (int v1, int v2) -> {
//            int suit1 = HandCards.getSuit(v1);
//            int point1 = HandCards.getPoint(v1);
//            int suit2 = HandCards.getSuit(v2);
//            int point2 = HandCards.getPoint(v2);
//            if (point2 < point1) return 1;
//            if (point2 > point1) return -1;
//            if (suit2 < suit1) return 1;
//            if (suit2 > suit1) return -1;
//            return 0;
//        });
//        System.out.println(Arrays.toString(arr));
//    }

}
