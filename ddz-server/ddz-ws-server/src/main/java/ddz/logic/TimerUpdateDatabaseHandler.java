package ddz.logic;

import ddz.constants.OpCode;
import ddz.db.dao.TableDao;
import ddz.db.dao.UserDao;
import com.kaka.notice.Command;
import com.kaka.notice.Message;
import com.kaka.notice.annotation.Handler;

/**
 * 将数据定时写入数据库，以减小数据库压力
 *
 * @author zkpursuit
 */
@Handler(cmd = OpCode.timer_writeto_db, type = String.class)
public class TimerUpdateDatabaseHandler extends Command {
    @Override
    public void execute(Message message) {
        boolean flush = false;
        if (message.getBody() instanceof Boolean) {
            flush = (Boolean) message.getBody();
        }
        UserDao userDao = this.retrieveProxy(UserDao.class);
        userDao.processAddOrUpdateQueue(true);
        TableDao tableDao = this.retrieveProxy(TableDao.class);
        tableDao.processAddOrUpdateQueue(true);
    }
}
