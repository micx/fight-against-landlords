package ddz.logic;

import com.kaka.notice.Command;
import com.kaka.notice.Message;
import com.kaka.notice.annotation.Handler;
import com.kaka.util.MathUtils;
import ddz.constants.OpCode;
import ddz.core.GameMode;
import ddz.db.service.IdGenService;
import ddz.db.service.MatcherService;

@Handler(cmd = OpCode.cmd_test_game, type = String.class)
public class TestGameHandler extends Command {
    @Override
    public void execute(Message message) {
        MatcherService matcher3Service = this.retrieveProxy(MatcherService.class);
        IdGenService idGenService = this.retrieveProxy(IdGenService.class);
        for (int i = 0; i < 13; i++) {
            matcher3Service.addWaitMatchPlayer(GameMode.NORMAL.getId(), 1, idGenService.nextId(), MathUtils.random(1000, 1010));
        }
        System.out.println("--------  开始匹配玩家");
    }
}
