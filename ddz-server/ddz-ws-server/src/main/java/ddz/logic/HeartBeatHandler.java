package ddz.logic;

import com.kaka.notice.annotation.Handler;
import ddz.constants.Crypto;
import ddz.constants.OpCode;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import ddz.protos.GlobalConn;

/**
 * 心跳包处理
 *
 * @author zkpursuit
 */
@Handler(cmd = OpCode.cmd_heart_beat, pooledSize = 1000)
public class HeartBeatHandler extends LogicDataHandler {

    @Override
    public void execute(ProtocolMessage msg) throws Exception {
        byte[] respBytes = GlobalConn.ScHeartBeat.newBuilder()
                .setTime(System.currentTimeMillis())
                .build().toByteArray();
        this.sendData(msg.ctx(), Crypto.isCrypto, Integer.parseInt(OpCode.cmd_heart_beat), respBytes);
    }

}
