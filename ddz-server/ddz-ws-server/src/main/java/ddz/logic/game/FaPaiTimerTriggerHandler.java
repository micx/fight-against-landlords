package ddz.logic.game;

import com.kaka.notice.Command;
import com.kaka.notice.Message;
import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.core.Table;
import ddz.core.TableState;
import ddz.db.dao.TableDao;
import ddz.net.WsSender;

/**
 * 发牌定时器触发后执行
 */
@Handler(cmd = OpCode.cmd_fapai)
public class FaPaiTimerTriggerHandler extends Command implements WsSender {
    @Override
    public void execute(Message msg) {
        long deskId = (long) msg.getBody();
        TableDao tableDao = this.retrieveProxy(TableDao.class);
        Table table = tableDao.getDesk(deskId);
        table.setState(TableState.qiang_dizhu);
        this.sendMessage(new Message(OpCode.cmd_show_opts, table)); //发完牌，显示庄家叫/不叫地主的按钮
    }
}
