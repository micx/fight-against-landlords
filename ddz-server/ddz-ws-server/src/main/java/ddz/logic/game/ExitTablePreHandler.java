package ddz.logic.game;

import ch.qos.logback.classic.Logger;
import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.core.Table;
import ddz.core.TableToken;
import ddz.db.service.TableTokenService;
import ddz.net.EventQueueExecutor;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import ddz.protos.GameExit;
import org.slf4j.LoggerFactory;

import java.util.Map;

@Handler(cmd = OpCode.cmd_exit_room)
public class ExitTablePreHandler extends LogicDataHandler {

    //private static final Logger logger = (Logger) LoggerFactory.getLogger(ExitTablePreHandler.class);

    @Override
    protected void execute(ProtocolMessage msg) throws Exception {
        long deskId = 0;
        if (msg.getBody() instanceof byte[]) {
            byte[] bytes = (byte[]) msg.getBody();
            GameExit.CsExit reqData = GameExit.CsExit.parseFrom(bytes);
            TableTokenService optTokenService = this.retrieveProxy(TableTokenService.class);
            TableToken tokenInfo = optTokenService.parseToken(reqData.getToken());
            deskId = tokenInfo.getDeskId();
            msg.setData(reqData);
        } else {
            Map<String, Object> paramMap = (Map<String, Object>) msg.getBody();
            if (paramMap != null) {
                Table table = (Table) paramMap.get("desk");
                deskId = table.getId();
            }
        }
        EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
        msg.setWhat(OpCode.cmd_exit_room);
        executor.addEvent(String.valueOf(deskId), msg);
    }
}
