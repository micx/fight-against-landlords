package ddz.logic.game;

import ch.qos.logback.classic.Logger;
import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.core.TableToken;
import ddz.db.service.TableTokenService;
import ddz.net.EventQueueExecutor;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import ddz.protos.GameTuoguan;
import org.slf4j.LoggerFactory;

@Handler(cmd = OpCode.cmd_trusteeship)
public class TuoGuanPreHandler extends LogicDataHandler {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(TuoGuanPreHandler.class);

    @Override
    protected void execute(ProtocolMessage msg) throws Exception {
        byte[] bytes = (byte[]) msg.getBody();
        GameTuoguan.CsTrustee reqData = GameTuoguan.CsTrustee.parseFrom(bytes);
        TableTokenService optTokenService = this.retrieveProxy(TableTokenService.class);
        TableToken tokenInfo = optTokenService.parseToken(reqData.getToken());
        long deskId = tokenInfo.getDeskId();
        if (deskId <= 0) {
            logger.error("玩家（" + msg.uid() + "）未加入任何房间");
            return;
        }
        EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
        msg.setWhat(OpCode.cmd_trusteeship);
        msg.setData(reqData);
        executor.addEvent(String.valueOf(deskId), msg);
    }
}
