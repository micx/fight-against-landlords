package ddz.logic.game;

import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.core.TableToken;
import ddz.db.service.TableTokenService;
import ddz.net.EventQueueExecutor;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import ddz.protos.GameReady;

@Handler(cmd = OpCode.cmd_ready)
public class ReadyPreHandler extends LogicDataHandler {

    @Override
    protected void execute(ProtocolMessage msg) throws Exception {
        byte[] bytes = (byte[]) msg.getBody();
        GameReady.CsReady reqData = GameReady.CsReady.parseFrom(bytes);
        if (reqData.getToken() != null && !"".equals(reqData.getToken())) {
            TableTokenService optTokenService = this.retrieveProxy(TableTokenService.class);
            TableToken tokenInfo = optTokenService.parseToken(reqData.getToken());
            long deskId = tokenInfo.getDeskId();
            if (deskId > 0) {
                EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
                msg.setWhat(OpCode.cmd_ready);
                msg.setData(reqData);
                executor.addEvent(String.valueOf(deskId), msg);
            } else {
                msg.setWhat(OpCode.cmd_ready);
                msg.setData(reqData);
                this.sendMessage(msg);
            }
        } else {
            msg.setWhat(OpCode.cmd_ready);
            msg.setData(reqData);
            this.sendMessage(msg);
        }
    }
}
