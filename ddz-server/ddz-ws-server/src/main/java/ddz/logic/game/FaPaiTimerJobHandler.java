package ddz.logic.game;

import com.kaka.notice.Command;
import com.kaka.notice.Message;
import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.net.EventQueueExecutor;

/**
 * 发牌定时处理，在此段时间内可以明牌操作可加倍
 *
 * @author zkpursuit
 */
@Handler(cmd = OpCode.timer_fapai, type = String.class)
public class FaPaiTimerJobHandler extends Command {

    @Override
    public void execute(Message message) {
        long deskId = (long) message.getBody();
        EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
        executor.addEvent(String.valueOf(deskId), new Message(Integer.parseInt(OpCode.cmd_fapai), deskId));
    }
}
