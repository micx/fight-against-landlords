package ddz.logic.game;

import ch.qos.logback.classic.Logger;
import com.kaka.notice.annotation.Handler;
import ddz.constants.OpCode;
import ddz.core.TableToken;
import ddz.db.service.TableTokenService;
import ddz.net.EventQueueExecutor;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import ddz.protos.GameJiabei;
import org.slf4j.LoggerFactory;

@Handler(cmd = OpCode.cmd_jiabei)
public class JiaBeiPreHandler extends LogicDataHandler {

    private static final Logger logger = (Logger) LoggerFactory.getLogger(JiaBeiPreHandler.class);

    @Override
    public void execute(ProtocolMessage msg) throws Exception {
        byte[] bytes = (byte[]) msg.getBody();
        GameJiabei.CsJiabei reqData = GameJiabei.CsJiabei.parseFrom(bytes);
        TableTokenService optTokenService = this.retrieveProxy(TableTokenService.class);
        TableToken tokenInfo = optTokenService.parseToken(reqData.getToken());
        long deskId = tokenInfo.getDeskId();
        if (deskId <= 0) {
            logger.error("玩家[{}]未加入任何房间，无须恢复房间数据", msg.uid());
            return;
        }
        EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
        msg.setWhat(OpCode.cmd_jiabei);
        msg.setData(reqData);
        executor.addEvent(String.valueOf(deskId), msg);
    }
}
