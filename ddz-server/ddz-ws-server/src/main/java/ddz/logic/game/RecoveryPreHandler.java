package ddz.logic.game;

import ch.qos.logback.classic.Logger;
import com.kaka.notice.annotation.Handler;
import ddz.constants.ErrCode;
import ddz.constants.ErrLevel;
import ddz.constants.OpCode;
import ddz.db.dao.UserDao;
import ddz.db.entity.UserInfo;
import ddz.net.EventQueueExecutor;
import ddz.net.LogicDataHandler;
import ddz.net.ProtocolMessage;
import org.slf4j.LoggerFactory;

@Handler(cmd = OpCode.cmd_recovery)
public class RecoveryPreHandler extends LogicDataHandler {

    private final static Logger logger = (Logger) LoggerFactory.getLogger(RecoveryPreHandler.class);

    @Override
    protected void execute(ProtocolMessage msg) throws Exception {
        UserDao userDao = this.retrieveProxy(UserDao.class);
        logger.warn("-------------" + msg.uid());
        UserInfo userInfo = userDao.getUserInfo(msg.uid(), "uid", "deskId");
        if (userInfo == null) {
            logger.error("玩家[{}]数据不存在", msg.uid());
            sendError(msg.ctx(), opcode(), ErrLevel.ERROR, ErrCode.data_err);
            return;
        }
        long deskId = userInfo.getDeskId();
        if (deskId <= 0) {
            logger.error("玩家[{}]未加入任何房间，无须恢复房间数据", msg.uid());
            return;
        }
        EventQueueExecutor executor = this.retrieveProxy(EventQueueExecutor.class);
        msg.setWhat(OpCode.cmd_recovery);
        msg.setData(userInfo);
        executor.addEvent(String.valueOf(deskId), msg);
    }
}
