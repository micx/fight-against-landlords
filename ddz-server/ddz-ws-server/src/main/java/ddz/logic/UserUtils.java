package ddz.logic;

import ddz.db.entity.UserInfo;
import ddz.numerical.manager.ConfPlaceInfoManager;
import ddz.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;

public class UserUtils {

    /**
     * 更新用户对局次数
     *
     * @param userInfo
     */
    public static void updateUserPlayCount(UserInfo userInfo, int mode, int place, ConfPlaceInfoManager manager) {
        int id = manager.isExists(mode, place);
        if (id <= 0) return;
        Map<Integer, Integer> map = parsePlayCountMap(userInfo.getPlayCounts());
        if (!map.containsKey(id)) {
            map.put(id, 1);
        } else {
            int count = map.get(id);
            count++;
            map.put(id, count);
        }
        userInfo.setPlayCounts(JsonUtils.toJsonString(map));
    }

    private static Map<Integer, Integer> parsePlayCountMap(String playCounts) {
        if (playCounts == null || "".equals(playCounts)) {
            return new HashMap<>(0);
        }
        return JsonUtils.toMap(playCounts, HashMap.class, Integer.class, Integer.class);
    }

    public static Map<Integer, Integer> getPlayCountMap(UserInfo userInfo) {
        return parsePlayCountMap(userInfo.getPlayCounts());
    }

    /**
     * 游戏场景中”再玩多少局“获得十局奖励
     *
     * @param userInfo
     * @param mode
     * @param place
     * @param manager
     * @return
     */
    public static int calcTenRemain(UserInfo userInfo, int mode, int place, ConfPlaceInfoManager manager) {
        Map<Integer, Integer> playCountMap = UserUtils.getPlayCountMap(userInfo);
        int placeId = manager.isExists(mode, place);
        if (placeId > 0) {
            if (!playCountMap.containsKey(placeId)) {
                return 10;
            } else {
                int playCount = playCountMap.get(placeId);
                int remain = 10 - playCount % 10;
                return remain;
            }
        }
        return 10;
    }

}
