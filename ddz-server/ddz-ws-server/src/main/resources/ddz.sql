use ddz;

drop table if exists user;
create table if not exists user
(
    uid bigint not null unique comment '用户唯一标识',
    unionid varchar(200) not null unique comment '登录码',
    sex int(1) not null default 1 comment '性别',
    nickname varchar(50) not null comment '昵称',
    headImg varchar(255) not null default '' comment '头像',
    winCount integer not null default 0 comment '胜利次数',
    totalCount integer not null default 0 comment '对局总数',
    score integer not null default 0 comment '积分',
    gold integer not null default 0 comment '游戏币',
    diam integer not null default 0 comment '钻石',
    fuka integer not null default 0 comment '福卡',
    serialWin integer not null default 0 comment '当前连胜次数',
    lastSerialWin integer not null default 0 comment '最后的连胜次数，一般用于使用连胜卡',
    maxSeriaWin integer not null default 0 comment '最大连胜次数',
    lianWin integer not null default 0 comment '连胜赛当前连胜次数',
    lastLianWin integer not null default 0 comment '连胜赛最后的连胜次数，一般用于使用连胜卡',
    maxLianWin integer not null default 0 comment '连胜赛最大连胜次数',
    deskId bigint not null default 0 comment '所在牌桌，如果正在对局则此值为非0值',
    createTimestamp bigint comment '创建时间戳',
    lastLoginTimestamp bigint comment '最后登录时间',
    items longtext comment '道具',
    token varchar(64) not null unique comment '登录令牌',
    primary key(uid)
)engine=innodb default charset=utf8 comment '用户表';

alter table user add gzh_openid varchar(36) comment '微信公众号openid';
alter table user add dailySign varchar(8) default '0000000' comment '7日签到';
alter table user add signDate bigint comment '最后签到时间戳';
alter table user add welfareDate bigint comment '每日福利最后领取时间戳';
alter table user add benefitCount integer comment '每日救济可领取次数';
alter table user add benefitDate bigint comment '救济领取时间';
alter table user alter benefitCount set default 2;
alter table user add firstRechDate bigint comment '首充时间';
alter table user add inviteAwardsDate bigint comment '领取邀请奖励的时间';
alter table user add playCounts varchar(1000) default '{}' comment '对局次数';
alter table user add lianWinDesk bigint default 0 comment '最后一局连胜模式下的桌子ID';
alter table user add lianWinReviveCount int(2) default 0 comment '连胜复活次数';

drop table if exists desk;
create table if not exists desk
(
    id bigint not null comment '唯一标识',
    mode integer not null comment '游戏玩法',
    place integer not null comment '玩法中的游戏场',
    maxPlayers integer not null comment '最大玩家人数',
    baseMultiple integer not null comment '基础倍率',
    bankerSeatIndex integer not null comment '庄家',
    players text comment '所有玩家信息',
    records longtext comment '所有玩家的操作记录',
    step integer not null comment '操作索引',
    state integer not null default 0 comment '牌桌状态',
    createTimestamp bigint comment '创建时间戳',
    fapaiStartTime bigint comment '发牌时间',
    laiziPoint integer not null default -1 comment '癞子牌点',
    primary key(id)
)engine=innodb default charset=utf8 comment '牌桌';

drop table if exists email;
create table if not exists email
(
    id bigint not null comment '唯一标识',
    sender bigint not null comment '发件人',
    senderName varchar(50) not null comment '发件人昵称',
    addressee bigint not null comment '收件人',
    addresseeName varchar(50) not null comment '收件人昵称',
    title varchar(50) not null comment '标题',
    content text not null comment '邮件正文',
    annex varchar(500) comment '邮件附件',
    sendTime datetime not null comment '邮件发送时间',
    validEndTime datetime comment '有效期结束时间',
    readTime datetime comment '读邮件时间',
    receiveTime datetime comment '领取附件时间',
    primary key(id)
)engine=innodb default charset=utf8 comment '邮件表';

drop table if exists email_history;
create table email_history select * from email where 1=2;

# drop procedure if exists del_email;
# delimiter ////
# create procedure del_email(in email_id integer)
# begin
#     insert into email (id,sender,senderName,addressee,addresseeName,title,content,annex,sendTime,validEndTime,readTime,receiveTime)
#     select id,sender,senderName,addressee,addresseeName,title,content,annex,sendTime,validEndTime,readTime,receiveTime from email where id=email_id;
#     delete from email where id=email_id;
# end ////
# delimiter ;

drop table if exists buy_log;
create table if not exists buy_log (
    id bigint not null auto_increment comment '主键',
    uid bigint not null comment '用户id',
    nickname varchar(50) not null comment '冗余用户昵称',
    headImg varchar(255) not null default '' comment '冗余用户头像',
    source int(2) not null comment '购买出处；1为商城',
    scid integer comment '出处配置ID',
    items varchar(255) not null default '[]' comment '购买的物品集合，json格式: [{cid:物品配置id, num:数量}]',
    cost decimal(11,2) not null default 0 comment '购买所花费的金额',
    currency integer not null default 0 comment '币种',
    buyDate datetime not null comment '购买日期',
    transactionId varchar(120) unique comment '交易号',
    swiftId varchar(120) unique comment '流水号',
    platform varchar(255) default null comment '充值平台',
    result int(1) default 0 comment '1为成功，其它为失败',
    fixDate datetime comment '确认支付时间',
    primary key(id)
) engine=innodb default charset=utf8 auto_increment=1 comment '购买记录表';

drop table if exists user_task;
create table if not exists user_task (
    id bigint not null auto_increment comment '主键',
    uid bigint not null comment '用户id',
    cid integer not null comment '任务配置ID',
    progress bigint not null default 0 comment '任务进度',
    createTimestamp bigint not null comment '创建时间',
    status bigint comment '领取奖励状态',
    primary key(id)
) engine=innodb default charset=utf8 auto_increment=1 comment '用户任务表';

drop table if exists friend;
create table if not exists friend
(
    id bigint not null comment '唯一标识，自增量',
    uid bigint not null comment '玩家ID',
    invitee varchar(200) unique comment '被邀请人的微信标识码unionid',
    fid bigint not null comment '好友ID',
    inviteTime datetime comment '邀请时间',
    fixedTime datetime comment '确定好友关系时间',
    receivedTime datetime comment '邀请的好友玩十局游戏后，本人领取奖励的时间',
    primary key(id)
)engine=innodb default charset=utf8 comment '用户好友表';

drop table if exists stop_server_log;
create table if not exists stop_server_log (
    id integer not null auto_increment comment '主键',
    stopTime datetime comment '停服时间',
    uids mediumtext comment '停服时在线的玩家ID列表',
    primary key(id)
) engine=innodb default charset=utf8 auto_increment=1 comment '停服记录表';

drop table if exists item_changed_log;
create table if not exists item_changed_log (
    id integer not null auto_increment comment '主键',
    opcode integer not null comment '操作ID',
    describe varchar(200) default '' comment '描述',
    uid integer not null comment '玩家ID',
    act tinyint not null comment '-1为消耗，1为获取',
    cid integer not null default 0 comment '道具配置ID',
    amount integer not null comment '消耗或者获得的数量',
    totals integer not null comment '消耗或者获得后的总量',
    logTime datetime not null comment '记录的时间',
    primary key(id)
) engine=innodb default charset=utf8 auto_increment=1 comment '消耗获得记录表';

# update user set deskId=0;
# delete from user_task;
# delete from buy_log;
# delete from desk;
# delete from email;
# delete from email_history;
# delete from friend;
# delete from user where uid>10000;

# 测试好友相关的奖励领取
# update user set inviteAwardsDate=null where uid=802194412024129;
# update friend set receivedTime=null where uid=802194412024129 and fid=3333314055464576